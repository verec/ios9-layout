//
//  AppTile.swift
//  iOS9-Y2
//
//  Created by verec on 30/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class AppTile: UIView {

    let textLabel = UILabel()

    convenience init(text: String) {
        self.init(frame:CGRect.zero)
        self.textLabel.text = text
        self.textLabel.textAlignment = .Center
        self.textLabel.sizeToFit()
        self.textLabel.layer.cornerRadius = 20.0
        let ligthestGray = UIColor(white: 0.965, alpha: 1.0)
        self.textLabel.layer.backgroundColor = ligthestGray.CGColor

        self.addSubview(self.textLabel)

        self.backgroundColor = UIColor.clearColor()
    }

    override init(frame: CGRect) {
        let insetFrame = frame.insetBy(dx: 20.0, dy: 20.0)
        super.init(frame: insetFrame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AppTile {

    override func layoutSubviews() {
        super.layoutSubviews()

        let bounds = self.bounds

        if bounds.isEmpty || bounds.isInfinite || bounds.isNull {
            return
        }

        let insetFrame = bounds.insetBy(dx: 20.0, dy: 20.0)
        self.textLabel.frame = insetFrame
        
    }
}