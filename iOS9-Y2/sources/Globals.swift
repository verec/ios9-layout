//
//  Globals.swift
//  iOS9-Y2
//
//  Created by verec on 19/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

struct Globals {

    static let version: String = {
                NSBundle.mainBundle()
                    .objectForInfoDictionaryKey("CFBundleShortVersionString") as! String
            }()
}

extension Globals {

    struct Assets {
        static let backgroundImage = UIImage(named: "main-background")
        static let mainWindowPattern = UIColor(patternImage:backgroundImage!)
    }

    struct Configuration {

        struct TileView {
            static let bellsAndWhistle          =   true
            static let interestingBackground    =   true
        }

        struct TouchBand {
            static let defaultBands             =   false
            static let uglyEllipse              =   false
            static let usesSlices               =   false
        }

        struct Application {
            static let usesCardDeck             =   true
            static let usesCardOrdering         =   true && usesCardDeck
        }
    }

    struct Tiling {
        static let tileSoure:TileSource     =   Configuration.Application.usesCardDeck
                                            ?   CardTileSource()
                                            :   AppTileSource()

        static let appTiler     = AppTiler(tileSource: tileSoure)
    }
}
