//
//  Tiling.swift
//  iOS9-Y2
//
//  Created by verec on 29/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

struct Tile {
    var view: UIView
}

protocol TileSource {
    var count: Int {get}
    func tileForIndex (index: Int) -> Tile
}

protocol TileOrdering {
    func successor(tileIndex: Int, horizontal: Bool, landscape: Bool) -> Int
    func predecessor(tileIndex: Int, horizontal: Bool, landscape: Bool) -> Int
}
