//
//  Tiler.swift
//  iOS9-Y2
//
//  Created by verec on 29/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class Tiler : UIView {

    struct DebugFlags {
        static let debugViewHierarchy       =   false
    }

    var lastBounds:         CGRect          = CGRect.zero
    var tileOrganizer:      TileOrganizer?

    var tileOrdering:TileOrdering {
        get {
            return tileOrganizer!.tileOrdering
        }
        set (newTileOrdering) {
            tileOrganizer!.tileOrdering = newTileOrdering
        }
    }

    convenience init(tileSource: TileSource) {
        self.init(frame: CGRect.zero, tileSource: tileSource)
    }

    convenience init(frame: CGRect, tileSource: TileSource) {
        self.init(frame: frame)
        self.tileOrganizer = TileOrganizer(tileSource: tileSource)
    }

    override init(frame: CGRect) {
        self.tileOrganizer = .None
        super.init(frame: frame)
        /// give actual tile views the control of transparency
        self.backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) will not be implemented, ever")
    }

    func pan(edge: UIRectEdge, panning: Panning, value: CGFloat) {
        extension_pan(edge, panning: panning, value: value)
    }

    func tap(edge: UIRectEdge) {
        extension_tap(edge)
    }
}

extension Tiler {

    func printSubviews() {
        print("view instances around: \(self.subviews.map{$0.description})")
    }
}

extension Tiler {

    func extension_tap(edge: UIRectEdge) {

        guard let tileOrganizer = self.tileOrganizer else { return }

        let (_,_,appear,vanish) = tileOrganizer.swapTileInfoForEdge(edge)

        if appear.view.superview == nil {
            self.addSubview(appear.view)
            self.sendSubviewToBack(vanish.view)
            self.insertSubview(appear.view, aboveSubview: vanish.view)
        }

        appear.view.frame = appear.before
        vanish.view.frame = vanish.before

        appear.view.alpha = 0.0
        vanish.view.alpha = 1.0

        tileOrganizer.currentIndex = appear.index

        func snapIntoPlace() {
            vanish.view.frame = vanish.after
            vanish.view.alpha = 0.0
            appear.view.frame = appear.after
            appear.view.alpha = 1.0
        }

        func cleanup(_ : Bool) {
            vanish.view.removeFromSuperview()
            if DebugFlags.debugViewHierarchy {
                printSubviews()
            }
        }

        let duration:NSTimeInterval = 0.3
        UIView.animateWithDuration(
            duration
        ,   animations: snapIntoPlace
        ,   completion: cleanup
        )
    }
}

extension Tiler {

    func extension_pan(edge: UIRectEdge, panning: Panning, value: CGFloat) {

        guard let tileOrganizer = self.tileOrganizer else { return }

        let (home,half,appear,vanish) = tileOrganizer.swapTileInfoForEdge(edge)

        if panning == .Began {

            if appear.view.superview == nil {
                self.addSubview(appear.view)
                self.sendSubviewToBack(vanish.view)
                self.insertSubview(appear.view, aboveSubview: vanish.view)
            }

            appear.view.alpha = 1.0
            vanish.view.alpha = 1.0
        }

        var appearRect = appear.before
        var vanishRect = vanish.before

        var keepAppearingWhenEnding:Bool = false

        if edge.horizontal {        /// eg Top/Bottom

            vanishRect.offsetInPlace(dx: 0.0, dy: value)
            appearRect.offsetInPlace(dx: 0.0, dy: value)

            let intersection = home.intersect(appearRect)
            keepAppearingWhenEnding = intersection.height > half

        } else if edge.vertical {   /// eg Left/Right

            vanishRect.offsetInPlace(dx: value, dy: 0.0)
            appearRect.offsetInPlace(dx: value, dy: 0.0)

            let intersection = home.intersect(appearRect)
            keepAppearingWhenEnding = intersection.width > half
        }

        if panning == .Ended {

            var viewToRemove: UIView? = .None

            func slideIntoPlace() {

                if keepAppearingWhenEnding {

                    appear.view.frame = home
                    vanish.view.frame = vanish.after
                    vanish.view.alpha = 0.0

                    tileOrganizer.currentIndex = appear.index

                } else {

                    appear.view.frame = appear.before
                    vanish.view.frame = home
                    appear.view.alpha = 0.0

                    tileOrganizer.currentIndex = vanish.index
                }
            }

            func cleanup(_:Bool) {

                if let view = viewToRemove where view == tileOrganizer.currentTile.view {
                    viewToRemove = .None
                }

                if let view = viewToRemove {
                    view.removeFromSuperview()
                    if DebugFlags.debugViewHierarchy {
                        printSubviews()
                    }
                }
            }

            if keepAppearingWhenEnding {
                viewToRemove = vanish.view
            } else {
                viewToRemove = appear.view
            }

            let duration:NSTimeInterval = 0.3
            UIView.animateWithDuration(
                duration
            ,   animations: slideIntoPlace
            ,   completion: cleanup
            )
        } else {
            vanish.view.frame = vanishRect
            appear.view.frame = appearRect
        }
    }
}

extension Tiler {

    override func layoutSubviews() {

        super.layoutSubviews()

        guard let tileOrganizer = tileOrganizer else { return }

        let view = tileOrganizer.currentTile.view
        if view.superview != self {
            self.addSubview(view)
            self.sendSubviewToBack(view)
        }

        /// check that the geometry is appropriate, fix it if not
        if self.lastBounds != self.bounds {
            tileOrganizer.layoutChanged(
                self.lastBounds
            ,   newBounds:  self.bounds
            ,   traits:     self.traitCollection)
            self.lastBounds = self.bounds
        }
    }
}