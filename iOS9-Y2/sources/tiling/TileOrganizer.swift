//
//  TileOrganizer.swift
//  iOS9-Y2
//
//  Created by verec on 29/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit


struct DefaultTileOrdering : TileOrdering {

    let tileSource: TileSource

    func fast_mod(x:Int, max:Int) -> Int {
        return (max + (x % max)) % max
    }

    func successor(tileIndex: Int, horizontal: Bool, landscape: Bool) -> Int {
        return fast_mod(tileIndex+1, max: tileSource.count)
    }

    func predecessor(tileIndex: Int, horizontal: Bool, landscape: Bool) -> Int {
        return fast_mod(tileIndex-1, max: tileSource.count)
    }
}

class TileOrganizer {

    var tileSource:     TileSource
    var tileOrdering:   TileOrdering

    init(tileSource: TileSource) {
        self.tileSource = tileSource
        self.tileOrdering = DefaultTileOrdering(tileSource: tileSource)
        self.currentIndex = 0
    }

    var currentIndex:Int = 0

    var currentTile: Tile {
        return tileSource.tileForIndex(currentIndex)
    }
}

extension TileOrganizer {

    func layoutChanged(oldBounds: CGRect, newBounds: CGRect, traits: UITraitCollection) {
        let view = currentTile.view
        view.sizeToFit()
        view.frame = newBounds
    }
}

extension TileOrganizer {

    func deviceOrientation() -> UIDeviceOrientation {
        let d = UIDevice.currentDevice()
        if !d.generatesDeviceOrientationNotifications {
            print("### starting device orientation events generation")
            d.beginGeneratingDeviceOrientationNotifications()
        }
        return d.orientation
    }

    func next(horizontal:Bool) -> Int {
        let o = deviceOrientation()
        return tileOrdering.successor(currentIndex, horizontal: horizontal, landscape: o.isLandscape)
    }

    func prev(horizontal:Bool) -> Int {
        let o = deviceOrientation()
        return tileOrdering.predecessor(currentIndex, horizontal: horizontal, landscape: o.isLandscape)
    }
}

extension TileOrganizer {

    func homeBounds() -> CGRect {
        let delegate = UIApplication.sharedApplication().delegate!
        let window = delegate.window!
        return window!.bounds
    }
}

extension TileOrganizer {

    func tileIndexForEdge(edge: UIRectEdge) -> Int {

        let lookup:[UIRectEdge:()->Int] = [
            UIRectEdge.Top:     { self.prev(false)  }
        ,   UIRectEdge.Left:    { self.prev(true)   }
        ,   UIRectEdge.Bottom:  { self.next(false)  }
        ,   UIRectEdge.Right:   { self.next(true)   }
        ]

        guard let eval = lookup[edge] else {
            return currentIndex
        }

        return eval()
    }
}

extension TileOrganizer {

    struct TileSpec {
        let index:  Int
        let view:   UIView
        let before: CGRect
        let after:  CGRect
    }

    typealias SwapTileSpec = (
        home:       CGRect
    ,   half:       CGFloat
    ,   appear:     TileSpec
    ,   vanish:     TileSpec
    )

    func swapTileInfoForEdge(edge: UIRectEdge) -> SwapTileSpec {

        let vnshIndx    = currentIndex
        let vnshView    = currentTile.view
        let apprIndx    = tileIndexForEdge(edge)
        let apprView    = tileSource.tileForIndex(apprIndx).view

//        let homeRect = homeBounds()
        let homeRect = vnshView.superview!.frame

        var apprBefr = homeRect
        let apprAftr = homeRect
        let vnshBefr = homeRect
        var vnshAftr = homeRect

        var half:CGFloat = 0.0

        let lookup:[UIRectEdge:()->()] = [

            UIRectEdge.Top:     {

                apprBefr.offsetInPlace(dx: 0.0, dy: -homeRect.height)
                vnshAftr.offsetInPlace(dx: 0.0, dy: homeRect.height)
                half = homeRect.midY - homeRect.minY
            }

        ,   UIRectEdge.Left:    {

                apprBefr.offsetInPlace(dx: -homeRect.width, dy: 0.0)
                vnshAftr.offsetInPlace(dx: homeRect.width, dy: 0.0)
                half = homeRect.midX - homeRect.minX
            }

        ,   UIRectEdge.Bottom:  {

                apprBefr.offsetInPlace(dx: 0.0, dy: homeRect.height)
                vnshAftr.offsetInPlace(dx: 0.0, dy: -homeRect.height)
                half = homeRect.midY - homeRect.minY
            }

        ,   UIRectEdge.Right:   {

                apprBefr.offsetInPlace(dx: homeRect.width, dy: 0.0)
                vnshAftr.offsetInPlace(dx: -homeRect.width, dy: 0.0)
                half = homeRect.midX - homeRect.minX
            }
        ]

        guard let eval = lookup[edge] else {
            fatalError("swapTileInfoForEdge called for an unsupported edge combination: \(edge.description)")
        }

        eval()

        return (
            home:   homeRect
        ,   half:   half
        ,   appear: TileSpec(index: apprIndx, view: apprView, before: apprBefr, after: apprAftr)
        ,   vanish: TileSpec(index: vnshIndx, view: vnshView, before: vnshBefr, after: vnshAftr)
        )
    }
}
