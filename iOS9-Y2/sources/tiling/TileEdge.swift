//
//  TileEdge.swift
//  iOS9-Y2
//
//  Created by verec on 17/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import CoreGraphics
import Foundation
import UIKit

enum Panning {
    case    Began
    case    Changed
    case    Ended
}

class TileEdge : UIView {

    typealias Panned            = (UIRectEdge, Panning, CGFloat) -> ()
    typealias Tapped            = (UIRectEdge) -> ()

    var panners:[Panned]        = []
    var tappers:[Tapped]        = []

    var points:[CGPoint]        = []

    var touchDown               = CGPoint.zero
    var horizontal:Bool?        = .None
    var lastPanValue:CGFloat    = 0.0
    var edge:UIRectEdge

    struct Parameters {
        static let bandCount:CGFloat    =   7.0
        static let bandColor            =   UIColor.cyanColor()
        static let bandSize:CGFloat     =   0.5
        static let halfPixel:CGFloat    =   0.5
    }

    convenience init(edge: UIRectEdge = .Top) {
        self.init(frame: CGRect.zero)
        self.edge = edge
    }

    override init(frame: CGRect) {
        self.edge = .None
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()

        let panner = UIPanGestureRecognizer()
        self.addGestureRecognizer(panner)
        panner.addTarget(self, action: Selector("panned:"))

        let tapper = UITapGestureRecognizer()
        self.addGestureRecognizer(tapper)
        tapper.addTarget(self, action: Selector("tapped:"))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var description:String {
        return "Band#\(edge.description)"
    }
}

extension TileEdge {

    @objc func tapped(reco: UIPanGestureRecognizer) {
        if reco.state == .Ended {
            tappers.forEach {
                $0(edge)
            }
        }
    }
}

extension TileEdge {

    @objc func panned(reco: UIPanGestureRecognizer) {

        func began() {
            touchDown = reco.translationInView(self.superview)
            horizontal = .None
            lastPanValue = 0.0
            panners.forEach {
                $0(edge, .Began, lastPanValue)
            }
        }

        func changed() {
            let touchNow = reco.translationInView(self.superview)
            let dx = touchNow.x - touchDown.x
            let dx2 = dx * dx
            let dy = touchNow.y - touchDown.y
            let dy2 = dy * dy

            if horizontal == .None {
                horizontal = dx2 > dy2
            }

            guard let horizontal = horizontal else { return }

            let value = horizontal ? dx : dy
            lastPanValue = value
            panners.forEach {
                $0(edge, .Changed, value)
            }
        }

        func ended() {
            panners.forEach {
                $0(edge, .Ended, lastPanValue)
            }
        }

        switch reco.state {
            case    .Began:     began()
            case    .Changed:   changed()
            case    .Ended:     ended()
            default:            break
        }
    }
}

extension TileEdge {

    override func layoutSubviews() {
        super.layoutSubviews()

        updateGeometry()
    }

    func updateGeometry() {

        self.points = []

        let bounds = self.bounds

        if bounds.isEmpty || bounds.isInfinite || bounds.isNull {
            return
        }

        let minX = bounds.minX
        let minY = bounds.minY
        let maxX = bounds.maxX
        let maxY = bounds.maxY


        if bounds.width > bounds.height {
            /// hz
            let size = bounds.height
            let gap = size / Parameters.bandCount

            for i in 0 ... Int(Parameters.bandCount) {
                let ySlot   = minY + gap*CGFloat(i)
                let start   = CGPoint(x:minX, y:ySlot)
                let end     = CGPoint(x:maxX, y:ySlot)

                self.points.append(start)
                self.points.append(end)
            }
        } else {
            /// vt
            let size = bounds.width
            let gap = size / Parameters.bandCount

            for i in 0 ... Int(Parameters.bandCount) {
                let xSlot   = minY + gap*CGFloat(i)
                let start   = CGPoint(x:xSlot, y:minY)
                let end     = CGPoint(x:xSlot, y:maxY)

                self.points.append(start)
                self.points.append(end)
            }
        }
    }
}

extension TileEdge {

    override func drawRect(rect: CGRect) {

        let context = UIGraphicsGetCurrentContext()

        CGContextSetStrokeColorWithColor(context, Parameters.bandColor.CGColor)
        CGContextSetLineWidth(context, Parameters.bandSize)
        CGContextStrokeLineSegments(context, points, points.count)
    }
}