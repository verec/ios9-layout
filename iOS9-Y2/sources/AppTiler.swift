//
//  ApplicationObjects.swift
//  iOS9-Y2
//
//  Created by verec on 22/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class AppTileEdge : TileEdge {

    override func drawRect(rect: CGRect) {

        /// either call super
        if Globals.Configuration.TouchBand.defaultBands {
            super.drawRect(rect)
        }

        /// or bail out if we're not drawing ourselves
        if !Globals.Configuration.TouchBand.uglyEllipse {
            return
        }

        /// now we're talking, but basic sanity checks first as we might
        /// be called at times when bounds is ill defined

        let bounds = self.bounds

        if bounds.isEmpty || bounds.isInfinite || bounds.isNull {
            return
        }

        let context = UIGraphicsGetCurrentContext()

        CGContextSetStrokeColorWithColor(context, TileEdge.Parameters.bandColor.CGColor)
        CGContextStrokeEllipseInRect(context, bounds)
    }
}

class AppTiler : Tiler {

    let topEdge = AppTileEdge(edge: .Top)
    let lftEdge = AppTileEdge(edge: .Left)
    let botEdge = AppTileEdge(edge: .Bottom)
    let rgtEdge = AppTileEdge(edge: .Right)

    override func layoutSubviews() {

        super.layoutSubviews()

        if topEdge.superview == nil {

            for separator in [topEdge, lftEdge, botEdge, rgtEdge] {
                self.addSubview(separator)
                separator.panners = [super.pan]
                separator.tappers = [super.tap]
            }
        }

        let bounds = self.bounds

        if Globals.Configuration.TouchBand.usesSlices {
            topEdge.frame = bounds.slice(slice: 1, outOf: 10, kind: .Wide)
            lftEdge.frame = bounds.slice(slice: 1, outOf: 10, kind: .Tall)
            botEdge.frame = bounds.slice(slice: 8, outOf: 10, kind: .Wide)
            rgtEdge.frame = bounds.slice(slice: 8, outOf: 10, kind: .Tall)
        } else {
            topEdge.frame = bounds.top(32.0, size: 44.0)
            lftEdge.frame = bounds.left(32.0, size: 44.0)
            botEdge.frame = bounds.bottom(32.0, size: 44.0)
            rgtEdge.frame = bounds.right(32.0, size: 44.0)
        }
    }
}

