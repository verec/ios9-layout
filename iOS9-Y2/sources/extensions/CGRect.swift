//
//  CGRect.swift
//  iOS9-Y2
//
//  Created by verec on 22/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import CoreGraphics
import UIKit.UIGeometry

extension CGRect {

    func positionned(intoRect intoRect: CGRect, widthUnitRatio: CGFloat, heightUnitRatio: CGFloat) -> CGRect {

        let intoSize:CGSize        = intoRect.size
        let intoCenter:CGPoint     = CGPoint(x: intoRect.minX + intoSize.width * widthUnitRatio
                                        ,    y: intoRect.minY + intoSize.height * heightUnitRatio)
        var rect = self
        rect.origin.x = intoCenter.x - (rect.width / 2.0)
        rect.origin.y = intoCenter.y - (rect.height / 2.0)

        return rect
    }

    func centered(intoRect intoRect: CGRect) -> CGRect {
        return self.positionned(intoRect: intoRect, widthUnitRatio: 0.5, heightUnitRatio: 0.5)
    }
}

extension CGRect {

    func top(offset: CGFloat, size: CGFloat) -> CGRect {
        var rect = self
        rect.origin.y = offset
        rect.size.height = size
        return rect
    }

    func left(offset: CGFloat, size: CGFloat) -> CGRect {
        var rect = self
        rect.origin.x = offset
        rect.size.width = size
        return rect
    }

    func bottom(offset: CGFloat, size: CGFloat) -> CGRect {
        var rect = self
        rect.origin.y = rect.size.height - (offset + size)
        rect.size.height = size
        return rect
    }

    func right(offset: CGFloat, size: CGFloat) -> CGRect {
        var rect = self
        rect.origin.x = rect.size.width - (offset + size)
        rect.size.width = size
        return rect
    }
}

extension CGRect {

    enum Slice {
        case    Wide
        case    Tall
    }

    func slice(slice slice:Int, outOf:Int, kind: Slice) -> CGRect {
        var rect = self
        switch kind {
            case    .Wide:  rect.size.height /= CGFloat(outOf)
                            rect.origin.y += rect.size.height * CGFloat(slice)
            case    .Tall:  rect.size.width /= CGFloat(outOf)
                            rect.origin.x += rect.size.width * CGFloat(slice)
        }
        return rect
    }
}