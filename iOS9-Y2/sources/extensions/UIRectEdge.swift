//
//  UIRectEdge.swift
//  iOS9-Y2
//
//  Created by verec on 29/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import CoreGraphics
import UIKit.UIGeometry

extension UIRectEdge {

    func string() -> String {

        switch self {
            case    UIRectEdge.None:    return "None"
            case    UIRectEdge.Top:     return "Top"
            case    UIRectEdge.Left:    return "Left"
            case    UIRectEdge.Bottom:  return "Bottom"
            case    UIRectEdge.Right:   return "Right"
            case    UIRectEdge.All:     return "All"
            default:                    return "Huh?"
        }
    }

    var description: String {
        return string()
    }

    func opposite() -> UIRectEdge {
        if [UIRectEdge.Left].contains(self) {
            return .Right
        } else if [UIRectEdge.Right].contains(self) {
            return .Left
        } else if [UIRectEdge.Top].contains(self) {
            return .Bottom
        }
        return .Top
    }

    var horizontal: Bool {
        return [UIRectEdge.Top, UIRectEdge.Bottom].contains(self)
    }

    var vertical: Bool {
        return [UIRectEdge.Left, UIRectEdge.Right].contains(self)
    }
}

extension UIRectEdge : Hashable {
    public var hashValue: Int {
        return Int(self.rawValue)
    }
}