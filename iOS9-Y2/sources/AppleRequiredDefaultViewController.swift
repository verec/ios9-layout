//
//  ViewController.swift
//  iOS9-Y2
//
//  Created by verec on 15/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import UIKit

class AppleRequiredDefaultViewController : UIViewController {

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.All]
    }
}
