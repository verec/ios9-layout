//
//  AppTileSource.swift
//  iOS9-Y2
//
//  Created by verec on 30/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

class AppTileSource : TileSource {

    var tiles:[Tile] = []
    var count: Int {
        return tiles.count
    }

    class Helper : UILabel {
        override var description:String {
            return self.text ?? "<null>"
        }
    }

    func helperLabel(title: String) -> UILabel {
        let label = Helper()
        label.text = title
        label.textAlignment = .Center
        label.sizeToFit()
        label.layer.borderWidth = 3.0
        label.layer.borderColor = UIColor.redColor().CGColor
        return label
    }

    init() {
        for i in 0 ... 9 {
            let view    =   Globals.Configuration.TileView.bellsAndWhistle
                        ?   AppTile(text: "AppTile #\(i)")
                        :   helperLabel("Tile#\(i)")

            let tile = Tile(view: view)
            tiles.append(tile)
        }
    }

    func tileForIndex (index: Int) -> Tile {
        return tiles[index]
    }
}
