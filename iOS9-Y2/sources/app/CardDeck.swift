//
//  CardDeck.swift
//  iOS9-Y2
//
//  Created by verec on 30/08/2015.
//  Copyright © 2015 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

struct CardDeck {

    static let cards:[String] = [
        "ace_of_clubs"      // 0
    ,   "ace_of_diamonds"   //
    ,   "ace_of_hearts"     //
    ,   "ace_of_spades2"    //
    ,   "2_of_clubs"        // 4
    ,   "2_of_diamonds"     //
    ,   "2_of_hearts"       //
    ,   "2_of_spades"       //
    ,   "3_of_clubs"        // 8
    ,   "3_of_diamonds"     //
    ,   "3_of_hearts"       //
    ,   "3_of_spades"       //
    ,   "4_of_clubs"        // 12
    ,   "4_of_diamonds"     //
    ,   "4_of_hearts"       //
    ,   "4_of_spades"       //
    ,   "5_of_clubs"        // 16
    ,   "5_of_diamonds"     //
    ,   "5_of_hearts"       //
    ,   "5_of_spades"       //
    ,   "6_of_clubs"        // 20
    ,   "6_of_diamonds"     //
    ,   "6_of_hearts"       //
    ,   "6_of_spades"       //
    ,   "7_of_clubs"        // 24
    ,   "7_of_diamonds"     //
    ,   "7_of_hearts"       //
    ,   "7_of_spades"       //
    ,   "8_of_clubs"        // 28
    ,   "8_of_diamonds"     //
    ,   "8_of_hearts"       //
    ,   "8_of_spades"       //
    ,   "9_of_clubs"        // 32
    ,   "9_of_diamonds"     //
    ,   "9_of_hearts"       //
    ,   "9_of_spades"       //
    ,   "10_of_clubs"       // 36
    ,   "10_of_diamonds"    //
    ,   "10_of_hearts"      //
    ,   "10_of_spades"      //
    ,   "jack_of_clubs2"    // 40
    ,   "jack_of_diamonds2" //
    ,   "jack_of_hearts2"   //
    ,   "jack_of_spades2"   //
    ,   "king_of_clubs2"    // 44
    ,   "king_of_diamonds2" //
    ,   "king_of_hearts2"   //
    ,   "king_of_spades2"   //
    ,   "queen_of_clubs2"   // 48
    ,   "queen_of_diamonds2"//
    ,   "queen_of_hearts2"  //
    ,   "queen_of_spades2"  //
    ,   "black_joker"       // 52
    ,   "red_joker"         //
    ]
}

class CardTile: UIView {
    var card:UIImageView?

    convenience init(cardName: String) {
        self.init(frame:CGRect.zero)
        let image = UIImage(named: cardName)!.resizableImageWithCapInsets(UIEdgeInsetsZero, resizingMode: .Stretch)
        self.card = UIImageView(image:image)
        self.addSubview(self.card!)

        self.backgroundColor = UIColor.clearColor()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CardTile {

    override func layoutSubviews() {
        super.layoutSubviews()

        let bounds = self.bounds

        if bounds.isEmpty || bounds.isInfinite || bounds.isNull {
            return
        }

        if let card = card {
            let insetFrame = bounds.insetBy(dx: 20.0, dy: 20.0)
            card.frame = insetFrame
        }
    }
}

class CardTileSource: TileSource {

    var tiles:[Tile] = []
    var count: Int {
        return tiles.count
    }

    func tileForIndex (index: Int) -> Tile {
        return tiles[index]
    }

    init() {
        self.tiles = CardDeck.cards.map { Tile(view:CardTile(cardName: $0)) }
    }
}

struct CardDeckOrdering : TileOrdering {

    let tileSource: TileSource

    init(tileSource: TileSource) {
        self.tileSource = tileSource
    }

    /// 0 - 51 cards, 52 = black Joker, 53 = red Joker
    func successor(tileIndex: Int, horizontal: Bool, landscape: Bool) -> Int {

        if horizontal {
            if tileIndex == 50 {
                return 51
            }
            if tileIndex == 51 {
                return 52
            }
            if tileIndex == 52 {
                return 53
            }
            if tileIndex >= 53 {
                return 0
            }
            return tileIndex + 4
        }

        // vertical next case
        if tileIndex == 52 {
            return 53
        }
        if tileIndex == 53 {
            return 0
        }

        if tileIndex >= 48 {
            return 52
        }

        let group = tileIndex / 4
        let newgp = (tileIndex + 1) / 4
        if group != newgp {
            return 0 + group * 4
        }
        return tileIndex + 1
    }

    func predecessor(tileIndex: Int, horizontal: Bool, landscape: Bool) -> Int {
        if horizontal {
            if tileIndex == 52 {
                return 51
            }
            if tileIndex >= 53 {
                return 52
            }
            if tileIndex == 4 {
                return 0
            }
            if tileIndex <= 3 {
                return 53
            }
            return tileIndex - 4
        }

        // vertical prev case
        if tileIndex >= 53 {
            return 52
        }
        if tileIndex == 52 {
            return 51
        }

        if tileIndex < 1 {
            return 53
        }

        let group = tileIndex / 4
        let newgp = (tileIndex - 1) / 4
        if group != newgp {
            return 3 + group * 4
        }

        return tileIndex - 1
    }
}

