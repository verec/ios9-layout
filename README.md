# An alternative navigation for iOS #

This is a navigation helper that manages full screen `Tile`s and replaces all sorts of ill designed view controllers. Provides an optional swipe and tap touch handling on each of the four edges of the screen (but far enough from the system defined areas)

### What do I need to import? ###

* `Tiling.swift`,
* `Tiler.swift`,
* `TileSeparator.swift` and
* `TileOrganizer.swift`.

The latter is required but you can otherwise ignore its existence.

* `UIRectEdge.swift` and `CGRect.swift` extensions.


### How do I use this? ###

* Get the files on your Mac.
* Incorporate them in your project as you see fit.
* Use the `App*` files as illustration and or the `CardDeck.swift`.
* Play with `Globals.swift`.

### I want to hear from you, but ###

* email preferred.
* _The bugs are 100% mine, the troubles are 100% yours._
* Take ownership! Understand the code and see whether it fits _your_ problem.

### Who do I talk to? ###

* Me, if you really need to, but otherwise, silence is golden.
* I'm sure you'll find ways to dig my email address if you really, positively, absolutely want to get in touch.

### The problem ###

How to get an `intuitive` navigation that isn't some kind of tree structure?
The issue with trees is that users get lost.

* "How many times do I have to press `back` to get back home?" and
* "Well, I'm home, now. How do I get to, you know, that screen where you can ... ?"
* You can't get a hint as to what a given sceen is, short of going into it, at which point, if it was the wrong one, your only option is to tap "back" and try again with some other screen.
* The tree navigation doesn't leverage our inate abilities at spatialisation. We know intuitively what's on the right, left, above or below. We do have a sense of proximity. But it breaks down fast when too many levels are involved.

### The solution ###

* Present the interface as full screen `tiles` that you can directly navigate with your thumb, by swiping them north, east, south or west, allowing you to peer at what that neighbour is before committing to it.
* You can both swipe (when uncertain) or tap (when you know your way around)
* What's next "to the right" doesn't need to be the same as what is next "to the bottom". In the card deck example, cards go fron Ace to Queen horozontally, but from spade to diamond vertically.

### The implementation ###

I could have made it all using a subclass of UIScrollView which does support paging. But I decided not to, because I already went that route once and regretted it deeply once the "solution" became too dependent on that implementation detail: it became a nightmare to tell a touch event meant for the current page from a touch event meant to activate a "paging event".

Hence why:

* I have defined hot areas (user configurable) with the `TileSeparator` (which
I admit is a bad name) which can, or not, have a visual representation, _so
that the user knows where to expect pagination to occur_.
* I have taken great care that, no matter how many tiles your app supports,
at most _two_ are needed at any one time, thus saving your app some memory pressure (iOS9 with is _two apps at once_ makes it even more necessary to be frugal on resource, expecially memory).
* I have let _you_ decide how to provide me tiles by relying on the `TileSource`
protocol. Whether _you_ hold to all your tiles or not is _your_ choice when
you implement it, As said above, I will at most retain _two_ tiles at once.
* I have let you decide what _next_ and _prev_ mean by letting you provide me with an optional `TileOrdering`. I do provide a default one _out of the box_ but it is rather cavalier and just rotates and wraps around, not distinguishing horizontal from vertical, let alone whether the device is in portrait or in landscape. But I have provided a less stupid one for you to play with that provides the card deck ordering.

Implememtation wise, when you do peer at the _next_ tile, if youy lift your finger, the tile will automatically go _where you intend it to go_, by using the heuristic that if you lift less than half way, it means *cancel* and if you lift more than halfway, I interpret it as *commit*.